cmake_minimum_required(VERSION 3.12)

project(win32-gl-triangle VERSION "0.1.0")

configure_file(config.h.in config.h)

add_executable(win32-gl-triangle
    include/app.hpp
    src/app-infra.cpp
    src/app.cpp
    src/glad.c
    src/glad_wgl.c
    src/program.cpp
)

target_compile_features(win32-gl-triangle
    PRIVATE
        cxx_nullptr
)

target_include_directories(win32-gl-triangle
    PRIVATE
        "include"
        "${PROJECT_BINARY_DIR}"
)

find_package(OpenGL REQUIRED)

target_link_libraries(win32-gl-triangle
    PRIVATE
        ${OPENGL_LIBRARIES}
)
