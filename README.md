# win32-gl-triangle

Simple Win32 project with the latest OpenGL using a load from https://glad.dav1d.de/. It will render a triangle (Whohoo!!)

![Screenshot](screenshot.png)
