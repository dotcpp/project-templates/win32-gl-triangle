#include <app.hpp>
#include <glad/glad.h>
#include <iostream>

#define GLSL(src) "#version 330 core\n" #src

void App::OnInit()
{
    float vertices[] = {
        0.0f, 0.5f,  // Vertex 1 (X, Y)
        0.5f, -0.5f, // Vertex 2 (X, Y)
        -0.5f, -0.5f // Vertex 3 (X, Y)
    };
    const char *vertexSource = GLSL(
        in vec2 position;

        void main() {
            gl_Position = vec4(position, 0.0, 1.0);
        });

    const char *fragmentSource = GLSL(
        out vec4 outColor;

        void main() {
            outColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
        });

    glClearColor(0.56f, 0.7f, 0.67f, 1.0f);

    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);

    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    auto vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertexSource, NULL);
    glCompileShader(vs);

    auto fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragmentSource, NULL);
    glCompileShader(fs);

    _shader = glCreateProgram();
    glAttachShader(_shader, vs);
    glAttachShader(_shader, fs);
    glBindFragDataLocation(_shader, 0, "outColor");
    glLinkProgram(_shader);

    glDeleteShader(vs);
    glDeleteShader(fs);

    GLint posAttrib = glGetAttribLocation(_shader, "position");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(posAttrib);
}

void App::OnFrame()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(_shader);

    glBindVertexArray(_vao);

    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void App::OnResize(int width, int height)
{
    glViewport(0, 0, width, height);
}

void App::OnExit()
{
    glDeleteProgram(_shader);
    glDeleteBuffers(1, &_vbo);
    glDeleteVertexArrays(1, &_vao);
}
